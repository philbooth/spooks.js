/**
 * This module exports functions for creating unit test spies and mock objects.
 */

/*globals define, module */

(function (globals) {
    'use strict';

    var functions, constants;

    functions = {
        ctor: mockConstructor,
        obj: mockObject,
        fn: mockFunction,
        mode: getMode
    };

    exportFunctions();

    /**
     * Public function `ctor`.
     *
     * Returns a unit test spy constructor, which returns mock instances that are
     * themselves unit test spies.
     *
     * @option name {object}           The name of the constructor being mocked, used
     *                                 as a key into the `log` object.
     * @option log {object}            Object used to store call counts, arguments
     *                                 and contexts, on properties `counts[name]`,
     *                                 `args[name]` and `these[name]` respecitvely.
     * @option archetype {object}      Archetype used to construct the mock instances
     *                                 that will be returned by the spy constructor.
     *                                 It must have either the property `instance`,
     *                                 an object that will be used as the template
     *                                 for mock instances, or the property
     *                                 `ctor`, a function that returns the
     *                                 template (usually this will be the constructor
     *                                 that is being mocked). If `ctor` is
     *                                 specified, the property `args` may also be set
     *                                 to specify the arguments to pass to that
     *                                 function.
     * @option [chains] {object}       Optional object containing flags indicating
     *                                 whether methods of the mock instances should
     *                                 be chainable. The flags are keyed by method
     *                                 name.
     * @option [results] {object}      Optional object containing values that will
     *                                 be returned from methods of the mock instances.
     *                                 The values are keyed by method name.
     * @options [callbacks] {function} Optional object containing functions to be
     *                                 called back methods of the mock instances are
     *                                 invoked. Useful when testing asynchronous code.
     * @option [mode] {mode}           Optional mode specifying how the mock instances
     *                                 should be created. For more about modes, see the
     *                                 public function `mode` at the bottom of this
     *                                 module.
     */
    function mockConstructor (options) {
        if (!isObject(options.results)) {
            options.results = {};
        }

        options.results[options.name] = mockObject({
            archetype: getArchetype(options.archetype),
            log: options.log,
            chains: options.chains,
            results: options.results,
            callbacks: options.callbacks,
            mode: options.mode
        });

        return mockFunction({
            name: options.name,
            log: options.log,
            results: options.results[options.name]
        });
    }

    function getArchetype (options) {
        if (isObject(options.instance)) {
            return options.instance;
        }

        if (isFunction(options.ctor)) {
            if (isArray(options.args)) {
                return instantiateWithArguments(options.ctor, options.args);
            }

            return new options.ctor();
        }

        throw new Error('Invalid archetype');
    }

    function isObject (thing) {
        return typeof thing === 'object' && thing !== null;
    }

    function isFunction (thing) {
        return typeof thing === 'function';
    }

    function isArray (thing) {
        return Array.isArray(thing);
    }

    function instantiateWithArguments (constructor, args) {
        Derived.prototype = constructor.prototype;
        return new Derived();

        function Derived () {
            return constructor.apply(this, args);
        }
    }

    /**
     * Public function `obj`.
     *
     * Returns a mock object that contains unit test spy methods.
     *
     * @option archetype {object}      Template object. The returned mock will have
     *                                 methods matching the archetype's methods.
     * @option log {object}            Object used to store spy method call counts,
     *                                 arguments and contexts, on the properties
     *                                 `counts`, `args` and `these` respectively.
     * @option [spook] {object}        Optional base object to add spy methods to.
     *                                 If not specified, a new clean object will
     *                                 be created instead.
     * @option [chains] {object}       Optional object containing flags indicating
     *                                 whether spy methods shoulds be chainable.
     *                                 The flags are keyed by method name.
     * @option [results] {object}      Optional object containing values that will
     *                                 be returned from spy methods. The values
     *                                 are keyed by method name.
     * @options [callbacks] {function} Optional object containing functions to be
     *                                 called back when the spy methods are invoked.
     *                                 Useful when testing asynchronous code.
     * @option [mode] {mode}           Optional mode specifying how the mock should be
     *                                 created. For more about modes, see the public
     *                                 function `mode`, at the bottom of this module.
     */
    function mockObject (options) {
        var archetype, spook;

        archetype = options.archetype;
        spook = options.spook || {};

        if (!isObject(archetype) && !isFunction(archetype)) {
            throw new Error('Invalid archetype');
        }

        if (!isObject(options.log)) {
            throw new Error('Invalid log');
        }

        // TODO: Should we also handle mocking arrays? (would just require using processArray at this level)

        processProperties(archetype, spook, {
            log: options.log,
            chains: options.chains || {},
            results: options.results || {},
            callbacks: options.callbacks || {},
            mode: options.mode || 0
        });

        return spook;
    }

    function processProperties (source, target, options) {
        var key;

        for (key in source) {
            if (shouldProcessProperty(source, key, options.mode)) {
                target[key] = processProperty(source[key], key, options);
            }
        }

        return target;
    }

    function shouldProcessProperty (object, key, mode) {
        if (object.hasOwnProperty(key) === false && shouldProcessPrototypes(mode) === false) {
            return false;
        }

        if (isObject(object[key])) {
            return shouldProcessObjects(mode);
        }

        if (!isFunction(object[key])) {
            return shouldProcessValues(mode);
        }

        return true;
    }

    function shouldProcessPrototypes (mode) {
        return isModeSet(constants.heavy, mode);
    }

    constants = {
        wide: 0x1,
        deep: 0x2,
        heavy: 0x4
    };

    function isModeSet (mode, currentModes) {
        /*jshint bitwise:false */

        return (mode & currentModes) === mode;
    }

    function shouldProcessObjects (mode) {
        return isModeSet(constants.deep, mode);
    }

    function shouldProcessValues (mode) {
        return isModeSet(constants.wide, mode);
    }

    function processProperty (property, name, options) {
        if (isFunction(property)) {
            return processProperties(property, mockFunction({
                name: name,
                log: options.log,
                chain: options.chains[name],
                results: options.results[name],
                callback: options.callbacks[name]
            }), options);
        }

        if (!isObject(property)) {
            return property;
        }

        if (isArray(property)) {
            return processArray(property, options);
        }

        return processProperties(property, {}, options);
    }

    function processArray (source, options) {
        var target = new Array(source.length);

        source.forEach(function (item, index) {
            target[index] = processProperty(item, index, options);
        });

        return target;
    }

    /**
     * Public function `fn`.
     *
     * Returns a unit test spy function.
     *
     * @option name {object}          The name of the function being mocked, used
     *                                as a key into the `log` object.
     * @option log {object}           Object used to store call counts, arguments
     *                                and contexts, on properties `counts[name]`,
     *                                `args[name]` and `these[name]` respecitvely.
     * @option [chain] {boolean}      Optional flag specifying whether the spy
     *                                function supports chaining (i.e. it returns
     *                                its own `this`). Defaults to `false`.
     * @option [results] {object}     Optional object containing values to be returned
     *                                by the spy function, keyed by `name`. Enables
     *                                result values to be set dynamically, after
     *                                the spy has been created.
     * @options [callback] {function} Optional function to be called back when the
     *                                spy function is invoked. Useful when testing
     *                                asynchronous code.
     */
    function mockFunction (options) {
        var name, chain, log, results, callback;

        name = options.name;
        chain = options.chain;
        log = options.log;
        results = options.results || [];
        callback = options.callback;

        if (typeof name !== 'string' || name === '') {
            throw new Error('Invalid function name');
        }

        if (callback && typeof callback !== 'function') {
            throw new Error('Invalid callback function');
        }

        if (results === undefined) {
            results = [];
        }

        if (!Array.isArray(results)) {
            results = [ results ];
        }

        initialiseLog({ counts: 0, args: [], these: [] });

        return function () {
            var resultIndex = log.counts[name] % results.length;

            logCall(arguments, this);

            if (callback) {
                callback();
            }

            if (chain === true) {
                return this;
            }

            return results[resultIndex];
        };

        function initialiseLog (values) {
            Object.keys(values).forEach(function (key) {
                initialiseLogProperty(key, values[key]);
            });
        }

        function initialiseLogProperty (key, value) {
            initialiseProperty(log, key, {});
            initialiseProperty(log[key], name, value);
        }

        function logCall (args, that) {
            log.counts[name] += 1;
            log.args[name].push(args);
            log.these[name].push(that);
        }
    }

    function initialiseProperty (object, property, value) {
        if (object[property] === undefined) {
            object[property] = value;
        }
    }

    /**
     * Public function `mode`.
     *
     * Returns a mode constant that can be used to modify the mocking behaviour of
     * other exported functions.
     *
     * @param modes {string} A comma-separated list of desired modes, combined in any
     *                       order. Valid modes are 'wide', 'deep' and 'heavy'. The
     *                       default mode implied when no modes are specified has a
     *                       constant value of zero and is assumed by every function.
     *                       It is both literally and conceptually the absence of the
     *                       other modes. The 'wide' mode indicates that mock objects
     *                       will be assigned copies of all of their archetype's own
     *                       non-object properties, not just the functions. The 'deep'
     *                       mode indicates that mock objects will be given the nested
     *                       object structure of their archetype. The 'heavy' mode
     *                       indicates that mock objects will be assigned properties
     *                       from the archetype's prototype chain, in addition to its
     *                       own properties. All combinations of modes are valid. Any
     *                       modes not recognised will cause an exception to be thrown.
     *                       Whitespace is ignored.
     */
    function getMode (modes) {
        /*jshint bitwise:false */

        var keys, result;

        keys = modes.split(',');
        result = 0;

        keys.forEach(function (key) {
            var mode = constants[key.trim()];

            if (typeof mode === 'undefined') {
                throw new Error('Invalid mode');
            }

            result |= mode;
        });

        return result;
    }

    function exportFunctions () {
      if (typeof define === 'function' && define.amd) {
          define(function () {
              return functions;
          });
      } else if (typeof module !== 'undefined' && module !== null && module.exports) {
          module.exports = functions;
      } else {
          globals.spooks = functions;
      }
    }
}(this));

